# BDP Development Environment


The purpose of this repository is to setup the BigDataPartnership Development environment.


These scripts are currently work in process but ideally will eventually support the following base operating Fedora 21 - (server or workstation).

To get started please run the following command which will bootstrap a freshly installed environment and then run the appropriate playbooks from within this repository.

    bash < <(curl -s https://bitbucket.org/bigdatapartnership/bdp-developer/raw/master/bootstrap.sh)

### Cavet ###
You do need to have **'curl'** installed first before running the above command.


### ISO's ###
Fedora Server: http://download.fedoraproject.org/pub/fedora/linux/releases/21/Server/x86_64/iso/Fedora-Server-DVD-x86_64-21.iso

Fedora Workstation: 
http://download.fedoraproject.org/pub/fedora/linux/releases/21/Workstation/x86_64/iso/Fedora-Live-Workstation-x86_64-21-5.iso

    ################################################################################
    #   Copyright 2015 Big Data Partnership Ltd                                    #
    #                                                                              #
    #   Licensed under the Apache License, Version 2.0 (the "License");            #
    #   you may not use this file except in compliance with the License.           #
    #   You may obtain a copy of the License at                                    #
    #                                                                              #
    #       http://www.apache.org/licenses/LICENSE-2.0                             #
    #                                                                              #
    #   Unless required by applicable law or agreed to in writing, software        #
    #   distributed under the License is distributed on an "AS IS" BASIS,          #
    #   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
    #   See the License for the specific language governing permissions and        #
    #   limitations under the License.                                             #
    ################################################################################