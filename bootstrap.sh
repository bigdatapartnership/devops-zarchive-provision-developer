#!/bin/bash -x

################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################
                                                                                                                                                                                                       
################################################################################ 
# Install Ansible and GIT to enable full installation of tools

cd ~
sudo easy_install ansible
sudo yum install -y git

if  [ ! -d "GIT" ]
then
    mkdir GIT
fi

cd GIT

if [ ! -d "bdp-developer" ]
then
    ( git clone https://bitbucket.org/bigdatapartnership/bdp-developer )
else
    ( cd bdp-developer; git reset --hard origin/master; git pull origin master )
fi


echo "Starting deployment via ansible"
cd ~/GIT/bdp-developer

read -p "Do you wish to setup the BDP development environment? " -n 1 -r REPLY  < /dev/tty
echo "You choose: $REPLY"
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Skipping Install Tools"
else
    ( ansible-playbook -i ansible/hosts ansible/playbooks/setup.yml )
fi

read -p "Do you wish to clone the BDP Public Repos? " -n 1 -r REPLY  < /dev/tty
echo "You choose: $REPLY"
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Skipping Clone Public Repos"
else
    ( ansible-playbook -i ansible/hosts ansible/playbooks/bdp_git_repos.yml )
fi

exit 0
